#language: fr

Fonctionnalité: Simple test pour l'utilisateur

  Scénario:
    Etant donné un utilisateur ayant comme nom "John"
    Quand je demande son nom
    Alors son nom est "John"